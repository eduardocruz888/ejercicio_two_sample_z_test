source('load_data.R')

grupo_retro <- group_by(estudiantes_2_intentos_grupos, Retroalimentacion)
estadisticos <- summarize(grupo_retro, Promedio_Global = mean(Calificacion_global), SD_Global = sd(Calificacion_global))
View(estadisticos)

# Incluir aquí, analisis two-sample z-test

#Ho: no tiene un efecto la retroalimentación del profesor en las calificaciones del estudiante versus la retroalimentación del sistema
#H1: la retroalimentación del profesor tiene un efecto en las calificaciones del estudiante versus la retroalimentación del sistema

N<-1543
n<-nrow(estudiantes_2_intentos_grupos)

SE_profesor <- estadisticos[1,3]/sqrt(n)
SE_sistema <- estadisticos[2,3]/sqrt(n)
SE_diff <- sqrt(SE_profesor^2+SE_sistema^2)
z <- ((estadisticos[1,2]-estadisticos[2,2])-0)/SE_diff
p_Value = pnorm(z[1,1]*-1)
print(p_Value)
# El p value es 0.18, por lo tanto no se rechaza la hipotesis nula y no se puede confirmar que la retroalimentación del profesor tiene un efecto en las calificaciones del estudiante versus la retroalimentación del sistema

